const assert = require('assert');
var marioChar = require('../mongodb/models/marioChar');
var mongoose = require('./connection')



describe('saves a record', () => {
    it('saves to mongoDB', (done) => {
        var char = new marioChar({
            name: "Mario",
            weight:200
        })

        char.save().then(() => {
            assert(char.isNew === false)
            done();
        });
    })
})
