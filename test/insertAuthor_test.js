const assert = require('assert');
var author = require('../mongodb/models/author');
var connection = require('./connection')
var mongoose = require('mongoose')



describe('Inserts A New Author', () => {
    let auth;
    let name = 'Orson Scott Card';
    let age = 67
    let books = [{
        Title: 'Enders Game',
        Pages: 600
    }, {
        Title: "Speaker For the Dead",
        Pages: 700
    }]
    beforeEach((next) => {
        mongoose.connection.collections.authors.drop(() => {
            next()
        })
    })
    beforeEach((done) => {
        auth = new author({
            Name: name,
            Age: age,
            Books: books
        })
        auth.save().then(() => {
            done();
        });
    })

    it('Verifies that Orson was saved', (done) => {
        author.findById(
            auth._id, (err, result) => {
                if (!err) {
                    assert(result._id.toString() == auth._id.toString(), result)
                    done()
                }
            })
    })

    it('Adds a Book', (done) => {
        author.findById(
            auth._id, (err, result) => {
                if (!err) {
                    result.Books.push({
                        Title: 'Xenocide',
                        Pages: 900
                    })
                    result.save().then(() => {
                        author.findById(result._id, (err, result) => {
                            assert(result.Books.length === 3, result)
                            done()
                        })
                    })

                }
            })
    })

})