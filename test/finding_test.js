const assert = require('assert');
var marioChar = require('../mongodb/models/marioChar');
var mongoose = require('./connection')



describe('finds a record', () => {
    var char;
    beforeEach((done) => {
        char = new marioChar({
            name: "Mario",
            weight: 200
        })
        char.save().then(() => {
            done();
        });
    })

    it('finds one from  mongoDB', (done) => {
        marioChar.find({
            name: 'Mario'
        }).then((data) => {
            assert(data[0].name == 'Mario', data.name)
            done()
        })
    })
    it('finds one by Id from  mongoDB', (done) => {
        marioChar.find({
            _id: char._id
        }).then((data) => {
            assert(data[0]._id.toString() == char._id.toString())
            done()
        })
    })
    
})