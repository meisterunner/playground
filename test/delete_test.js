const assert = require('assert');
var marioChar = require('../mongodb/models/marioChar');
var mongoose = require('./connection')



describe('finds a record', () => {
    var char;

    beforeEach((done) => {
        char = new marioChar({
            name: "Mario",
            weight: 200
        })
        char.save().then(() => {
            done();
        });
    })
    it('removes a record', (done) => {
        marioChar.findOneAndRemove({
            _id: char._id
        }).then((data) => {
            assert(data != null)
            done();
        })
    })

})