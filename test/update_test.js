const assert = require('assert');
var marioChar = require('../mongodb/models/marioChar');
var mongoose = require('./connection')



describe('Updates a record', () => {
    var char;

    beforeEach((done) => {
        char = new marioChar({
            name: "Mario",
            weight: 200
        })
        char.save().then(() => {
            done();
        });
    })

    it('updates a record name', (done) => {
        marioChar.findByIdAndUpdate({
            _id: char._id
        }, {
            name: 'luigi'
        }).then((data) => {
            marioChar.findById(data._id, (err, res) => {
                if (!err) {
                    assert(res.name=='luigi',res)
                }
                done();
            })
        })
    })

})