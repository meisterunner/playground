const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const MarioCharSchema = new Schema({
    name: String,
    weight: Number
})

module.exports = MarioChar = mongoose.model("mariochar", MarioCharSchema)