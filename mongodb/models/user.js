const mongoose = require('mongoose')
const Schema = mongoose.Schema;



const UserSchema = new Schema({
    FirstName: String,
    LastName: String,
    Age: Number,
    Gender: String,
    OauthId: String,
    Email:String
})

module.exports = mongoose.model("User", UserSchema)