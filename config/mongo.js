var mongoose = require('mongoose')

mongoose.Promise = global.Promise;

module.exports = (req,res,next) => {
    mongoose.connect('mongodb://localhost/test');
    mongoose.connection.once('open', () => {
        console.log('Connection Open');
        next();
    }).on('err', (err) => {
        console.log(err, "MongoDB Connection Error");
        next()
    })
}

