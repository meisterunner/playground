const passport = require('passport')
const google = require('passport-google-oauth20')
const facebook = require('passport-facebook')
const mongoose = require('mongoose')
const MongoConnection = require("./mongo")
const user = require('../mongodb/models/user')

//serializer
passport.serializeUser(function (user, done) {

    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});

passport.use(new facebook({
    clientID: process.env.FACEBOOK_APP_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: "/auth/facebook/redirect",
    profileFields: ['id', 'displayName', 'photos', 'email', 'name', 'gender', 'profileUrl', 'address', 'birthday', 'location']
}, (accessToken, refreshToken, profile, done) => {
    console.log(profile);

    saveUser(profile, done)
}))

passport.use(new google({
    //Options for google strat
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: "/auth/google/redirect"
}, (accessToken, refreshToken, profile, done) => {
    console.log(profile);
    // //passport Callback Function 
    saveUser(profile, done)
}))

function saveUser(profile, done) {
    let person = new user({
        FirstName: profile.name.givenName,
        LastName: profile.name.familyName,
        Gender: profile.gender,
        OauthId: profile.id,
        Email: profile.emails[0].value
    })

    MongoConnection(null, null, () => {
        user.findOne({
            OauthId: profile.id
        }, (err, result) => {
            if (!err) {
                save(result);
            } else {
                console.log(err);
            }
        })
    })

    function save(result) {
        if (result == null) {
            console.log(result);
            person.save().then(() => {
                done(null, profile);
            });
        } else {
            //Check for updates
            user.findOne({
                OauthId: person.OauthId
            },(err,result)=>{
                if(!err){
                    console.log(result);                    
                }
            })
            done(null, profile);
        }
    }
}