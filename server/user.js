class User {
    constructor(email, name) {
        this.email = email;
        this.name = name;
    }
    login() {
        return 'Logged in'
    }
    logout() {
        return 'Logged out'
    }
}
var users = [];
var newUser = (email, name) => {
    var user = new User(email, name);
    var duplicate = 0;
    for (let i = 0; i < users.length; i++) {
        if (users[i].email == user.email) {
            duplicate = 1;
        }
    }
    if (duplicate == 0) {
        users.push(user);
    }
}

module.exports = {
    user: newUser,
    users: users
}