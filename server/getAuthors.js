const mongoose = require('mongoose')
const authors = require('../mongodb/models/author')

module.exports = (req, res, next) => {
    authors.find((err, result) => {
        if (!err) {
            res.locals.authors = result
            next();
        }
    })
}