//#region Imports
var express = require('express');
var router = express.Router();
var user = require('../server/user')
var request = require('request')
var connectToMongo = require('../config/mongo');
const authors = require('../server/getAuthors');
var fill = require('../server/pdf').fill

//#endregion

//#region GET
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {
        title: 'Express'
    });
});
router.get('/balls', function (req, res, next) {
    res.render('balls', {
        title: 'Balls'
    });
});
router.get('/videoSign', function (req, res, next) {
    res.render('videoSigs', {
        title: 'Video Signature'
    });
});
router.get('/circularMotion', function (req, res, next) {
    res.render('circularMotion', {
        title: 'circular Motion'
    });
});
router.get('/tetris', function (req, res, next) {
    res.render('tetris', {
        title: 'tetris'
    });
});
router.get('/konami', function (req, res, next) {
    res.render('konami', {
        title: 'konami'
    });
});
router.get('/jsClasses', function (req, res, next) {

    res.render('jsClasses', {
        title: 'jsClasses',
        user: null
    });
});
router.get('/logout', (req, res) => {
    req.logOut();
    res.redirect('/')
})
router.get('/recaptcha', (req, res) => {
    res.render('recaptcha', {
        title: 'reCaptcha'
    })
})
router.get('/mongo', connectToMongo, authors, (req, res, next) => {
    res.render('mongo', {
        title: "MongoDB",
        authors: res.locals.authors

    })
})
router.get('/pdfFiller', (req, res) => {
    res.render('pdffiller', {
        title: "PDF Filler"
    })
})
router.get('/asyncawait', (req, res, next) => {
    async function g() {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log(2)
            }, 1000);

        })
    }
    async function f() {
        let promise = new Promise((resolve, reject) => {
            setTimeout(() => resolve(1), 3000)
        });

        let result = await promise; // wait till the promise resolves (*)
        console.log(result);
        g()
    }
    f().then(() => {
        res.render('asyncawait', {
            title: "async TEst"

        })
    });
})
router.get('/chart', (req, res, next) => {
    let months = 49;
    let startBal = 10000;
    let rate = .035;
    try {

        if (req.query.months) {
            months = parseInt(req.query.months)
        }
        if (req.query.startBal) {
            startBal = parseFloat(req.query.startBal)
        }
        if (req.query.rate) {
            rate = parseFloat( req.query.rate)
            if (rate > 0) {
                rate = rate * .010
            }
        }
    } catch (error) {
        console.log(error);   

    }
    rateCalculator(rate, startBal, months).then((result) => {
        res.locals.balance = result
        res.render('chart', {
            title: "Chart Demo",
            months: months,
            balance: res.locals.balance
        })
    })
})
router.get('/customChart', (req, res, next) => {
    rateCalculator(.035, 10000, 49).then((result) => {
        res.render('lineChart', {
            title: 'Custom Chart',
            periods: 49,
            balance: result
        })
    })
})

//#endregion

//#region POST

router.post('/newUser', function (req, res, next) {
    user.user(req.body.email, req.body.name)
    console.log(user.users)
    res.send(user.users);
});
router.post('/', function (req, res, next) {
    console.log(req.body)
    setTimeout(function () {
        res.send('POsted');
    }, 5000)
});
router.post('/recaptcha', (req, res) => {
    if (req.body.captcha == undefined || req.body.captcha == null || req.body.captcha == '') {
        return res.json({
            "success": false,
            "msg": "Captcha did not capture"
        })
    } else {
        //Send to Google
        const verifyURL = `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${req.body.captcha}&remoteIp=${req.connection.remoteAddress}`
        request(verifyURL, (err, response, data) => {
            data = JSON.parse(data)
            if (data.success == true) {
                res.send(true)
            } else {
                res.send(false)
            }
        })
    }
})
//#endregion

module.exports = router;

async function rateCalculator(percentage, startBal, term) {
    let balance = startBal;
    let results = [];
    results.push(balance)
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2
    })

    for (let i = 0; i < term; i++) {
        balance = await (balance * percentage) + balance;
        // if (i == 21 || i == 25 || i==35) {
        //     balance = balance - 5000
        // }
        try {
            var formattedBalance = balance.toString().split('.')

            if (typeof formattedBalance[1] == 'undefined') {
                formattedBalance[1] = '00'
            } else {
                formattedBalance[1] = formattedBalance[1].substr(0, 2)
            }

        } catch (error) {

        }
        // var formattedBalance = await formatter.format(balance)
        results.push(parseFloat(formattedBalance[0] + '.' + formattedBalance[1]))
    }

    return results;

}