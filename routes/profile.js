var router = require('express').Router()
var isLoggedin = require('../server/isLoggedIn')


router.get('/', isLoggedin, (req, res, next) => {
    res.render('profile/profile', {
        title: 'Profile',
        user: req.user
    })
})

module.exports = router;