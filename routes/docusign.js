var express = require('express');
var router = express.Router();
const docusign = require('docusign-esign');
const apiClient = new docusign.ApiClient();

const integratorKey = 'f079b042-4386-485a-a339-a59dca145b36'; // An IK for a non-mobile docusign account
const clientSecret = 'd0353bdb-4f40-42b3-8e54-0c292b03ee8a';
const redirectUri = 'https://localhost:3000/docusign/auth'; // This needs to be registered with the integrator key in your admin account
const basePath = 'https://demo.docusign.net/restapi';
apiClient.setBasePath(basePath);
const responseType = apiClient.OAuth.ResponseType.CODE; // Response type of code, to be used for the Auth code grant
const scopes = [apiClient.OAuth.Scope.EXTENDED];
const randomState = "*^.$DGj*)+}Jk";


/* GET / */
router.get('/', (req, res, next) => {
    const authUri = apiClient.getAuthorizationUri(integratorKey, scopes, redirectUri, responseType, randomState); //get DocuSign OAuth authorization url
    //Open DocuSign OAuth login in a browser, res being your node.js response object.
    res.redirect(authUri);
});
router.get('/auth', (req, res, next) => {
    // IMPORTANT: after the login, DocuSign will send back a fresh
    // authorization code as a query param of the redirect URI.
    // You should set up a route that handles the redirect call to get
    // that code and pass it to token endpoint as shown in the next
    // lines:
    apiClient.generateAccessToken(integratorKey, clientSecret, req.query.code, function (err, oAuthToken) {

        console.log('OauthToken', oAuthToken.accessToken);

        //IMPORTANT: In order to access the other api families, you will need to add this auth header to your apiClient.
        apiClient.addDefaultHeader('Authorization', 'Bearer ' + oAuthToken.accessToken);

        apiClient.getUserInfo(oAuthToken.accessToken, function (err, userInfo) {
            console.log("UserInfo: ", userInfo);
            // parse first account's baseUrl
            // below code required for production, no effect in demo (same
            // domain)
            apiClient.setBasePath(userInfo.accounts[0].baseUri + "/restapi");
            signDoc(userInfo)
        });
    });

    function signDoc(userInfo) {
        // create the signature request
        // create a new envelope object that we will manage the signature request through
        var envDef = new docusign.EnvelopeDefinition();
        envDef.emailSubject = 'Please sign this document sent from Node SDK';
        envDef.templateId = 'b3c190d1-21ae-4dc0-b69a-7c73c5da7d0c';

        // create a template role with a valid templateId and roleName and assign signer info
        var tRole = new docusign.TemplateRole();
        tRole.roleName = 'Signer1';
        tRole.name = 'John Meister';
        tRole.email = 'john.meister@silverrockinc.com';
        var tRole2 = new docusign.TemplateRole();
        tRole.roleName = 'Signer2';
        tRole.name = 'John M';
        tRole.email = 'john.d,meister@outlook.com';

        // set the clientUserId on the recipient to mark them as embedded (ie we will generate their signing link)
        tRole.clientUserId = '1001';

        // create a list of template roles and add our newly created role
        var templateRolesList = [];
        templateRolesList.push(tRole);
        templateRolesList.push(tRole2);

        // assign template role(s) to the envelope
        envDef.templateRoles = templateRolesList;

        // send the envelope by setting |status| to 'sent'. To save as a draft set to 'created'
        envDef.status = 'sent';

        // use the |accountId| we retrieved through the Login API to create the Envelope
        var accountId = "8502c92a-0083-46cf-8957-b49787d38258" || userInfo.accounts[0].accountId;

        // instantiate a new EnvelopesApi object
        var envelopesApi = new docusign.EnvelopesApi();

        // call the createEnvelope() API
        envelopesApi.createEnvelope(accountId, {
            'envelopeDefinition': envDef
        }, function (err, envelopeSummary, response) {
            if (err) {
                return next(err);
            }
            console.log('EnvelopeSummary: ' + envelopeSummary);
            res.json(envelopeSummary)
        });

    }
});


module.exports = router;