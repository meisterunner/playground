var router = require('express').Router();
const passport = require('passport')
var isLoggedIn = require('../server/isLoggedIn')

router.get('/login', (req, res, next) => {
    res.render('auth/login', {
        user: req.user
    })
})

router.get('/logout', (req, res, next) => {
    req.logout();
    res.redirect('/')
})

router.get('/google', passport.authenticate('google', {
    scope: ['profile', 'email']
}))
router.get('/google/redirect', (req, res, next) => {
    function login(err, user) {
        if (err) console.log(err);
        req.logIn(user, (err) => {
            if (err) console.log(err);
            return res.redirect('/profile/')
        })
    }
    passport.authenticate('google', login)(req, res, next)
})

router.get('/facebook', passport.authenticate('facebook', {
    authType: 'rerequest',
     scope: ['email', 'public_profile']
}))
router.get('/facebook/redirect', (req, res, next) => {
    function login(err, user) {
        if (err) console.log(err);
        req.logIn(user, (err) => {
            if (err) console.log(err);
            // return res.redirect('/profile/')
            let profile = encodeURIComponent(user)
            console.log('profile Information: ',user);
            
            return res.redirect('/auth/register')
        })
    }
    passport.authenticate('facebook', login)(req, res, next)
})
router.get('/register',(req,res)=>{
    console.log(req.query);
    res.render('auth/register',{title:'Register',user:null})
    
})

module.exports = router;