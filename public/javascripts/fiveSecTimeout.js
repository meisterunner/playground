$('form').submit(function (e) {
    e.preventDefault();
    var body = {
        name: $('#name').val()
        , phone: $('#phone').val()
    }
    console.log(body)
    $.post('/', body, function (data) {
        console.log(data);
    })
})
$( document ).ajaxStart(function() {
    $( ".spinner" ).show( );
  });
  $( document ).ajaxComplete(function() {
    $( ".spinner" ).fadeOut(1000,function(){
        $( ".success" ).show();
    });
  });