﻿var canvas = document.querySelector('canvas');
canvas.width = (window.innerWidth);
canvas.height = (window.innerHeight);
var c = canvas.getContext('2d');
var gravity = 1;
var mouse = {
    x : undefined,
    y: undefined
}
addEventListener('resize', function () {
    canvas.width = (window.innerWidth);
    canvas.height = (window.innerHeight);
    init();
})
addEventListener('mousemove', function (e) {
    mouse.x = e.x;
    mouse.y = e.y;

})
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
function randomColor() {
    var r = randomInt(0, 100);
    var g = randomInt(0, 100);
    var b = randomInt(150, 255);
    return "rgb(" + r + "," + g + "," + b + ")";
}
class Object {
    constructor(x, y, radius, color) {
        this.x = x;
        this.y = y;       
        this.radius = radius
        this.color = color  
        this.radians = Math.random() * (Math.PI * 2);
        this.velocity = 0.025;
        this.distanceFromCenter = randomInt(50, 120)
        this.update = function () {
            ////move points
            this.radians += this.velocity
            ////circular motion

            if (radius == 1) {
                this.radians += this.velocity+.05
                this.x = x + Math.cos(this.radians) * randomInt(5, 20)
                this.y = y + Math.sin(this.radians) * randomInt(5, 20)
            }
            if (radius == 3) {
                this.radians += this.velocity - .005
                this.x = x + Math.cos(this.radians) * this.distanceFromCenter
                this.y = y + Math.sin(this.radians) * this.distanceFromCenter
            }
            
            
            this.draw();
        }
        this.draw = function () {
            c.beginPath();
            c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
            c.fillStyle = this.color;
            c.fill();
            c.closePath();
        }
    }
}
var objectArray = [];

function init() {
    objectArray = [];
    for (let i = 0; i < 8; i++) {
        var radius = randomInt(1,7);       
        var x = canvas.width / 2
        var y = canvas.height / 2
        var color = randomColor();
        objectArray.push(new Object(x, y, radius, color))
        console.log(x, y)

    }    
}
function animate() {
    c.clearRect(0, 0, canvas.width, canvas.height);
    for (let index = 0; index < objectArray.length; index++) {
        objectArray[index].update();
    }
      c.beginPath();
    c.arc(canvas.width / 2, canvas.width / 2, 10, 0, Math.PI * 2, false)
    c.fillStyle = "orangered";
    c.fill();
    c.closePath();
    requestAnimationFrame(animate);

}
init();
animate();