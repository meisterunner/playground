﻿var canvas = document.querySelector('canvas');
canvas.width = (window.innerWidth);
canvas.height = (window.innerHeight);
var c = canvas.getContext('2d');
 var mouse={
     x:undefined,
     y:undefined
 }
 var gravity = 1;
 
addEventListener('resize',function(){
    canvas.width = (window.innerWidth);
    canvas.height = (window.innerHeight);   
    init(); 
})
function randomInt(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}
function randomColor(){
var r = randomInt(0,255);
var g = randomInt(0,255);
var b = randomInt(0,255);
return "rgb("+r+","+g+","+b+")";
}

class Object{
    constructor(x,y,dx,dy,radius,color){
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy
        this.radius=radius
        this.color=color
        var friction = 0.9;
        this.update = function(){
            if (this.y + this.radius + this.dy > (canvas.height)) {                
                this.dy = -this.dy * friction;
            }
            else{
                this.dy += gravity;
            }
            if (this.x + this.radius + this.dx > canvas.width || this.x - this.radius <= 0) {
                               
                this.dx = -this.dx  ;
                
            }           
            this.x += this.dx;
            this.y += this.dy;
            this.draw();
        }
        this.draw = function(){
            c.beginPath();
            c.arc(this.x,this.y,this.radius,0,Math.PI*2,false)
            c.fillStyle =this.color;
            c.fill();
            c.closePath();
        }
    }
}

var objectArray= [];
function init(){
    objectArray= [];
    for (let i = 0; i < 2; i++) {
        var radius = randomInt(4,30);
        var x = randomInt(radius,canvas.width-radius)
        var y = randomInt(radius,canvas.height-500)
        var dy = randomInt(0, 5) - 0.5;
        var dx = randomInt(0, 5) - 0.5;
        var color = randomColor();        
        objectArray.push(new Object(x,y,dx,dy,radius,color))
    }
}
function animate(){    
    c.clearRect(0, 0,canvas.width,canvas.height);
    for (let index = 0; index < objectArray.length; index++) {
        objectArray[index].update();
    }  
    requestAnimationFrame(animate);   
    console.log(objectArray.length);
}
init();
animate();

addEventListener('mousemove', function (e) {
    var radius = randomInt(4, 30);
    var x = e.x;
    var y = e.y;
    var dy = randomInt(0, 5) - 0.5;
    var dx = randomInt(0, 5) - 0.5;
    var color = randomColor();    
    if (objectArray.length >1000) {
        objectArray.splice(0, 1);
    }
    objectArray.push(new Object(x, y, dx, dy, radius, color));
})
function getDistance(x1, x2, y1, y2) {
    let xd = x2 - x1;
    let yd = y2 - y1;
    var distance = Math.sqrt((Math.pow(xd, 2)) + (Math.pow(yd, 2)));
    return distance;
}